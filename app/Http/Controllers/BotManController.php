<?php

namespace App\Http\Controllers;

use App\Signal;
use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;
use Illuminate\Support\Facades\Log;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

        $botman->hears('{ticker}-3min', 'App\Http\Controllers\BotManController@min3');

        $botman->listen();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }

    public function min3(BotMan $bot, $ticker) {
        $signal = Signal::where('TICKER', $ticker)
            ->orderBy('id', 'desc')
            ->first();

        if (!is_null($signal)) {
            $bot->reply(
                $signal->TICKER .', '
                .$signal->GAP .', '
                .$signal->ENTRY3 .', '
                .$signal->STOPLOST3 .', '
                .$signal->TSL3 .', '
                .$signal->TAGET13 .', '
                .$signal->TAGET23 .', '
                .$signal->TAGET33 .', '
                .$signal->RSI3 .', '
                .$signal->PC3 .', '
                .$signal->FC3 .', '
                .$signal->LC3 .', '
                .$signal->SHOW3 .', '
            );
        }
    }
}
