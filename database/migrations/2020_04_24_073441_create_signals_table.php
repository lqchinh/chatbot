<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signals', function (Blueprint $table) {
            $table->increments('id');

            $table->date('SIGNDATE')->nullable();
            $table->time('SIGNTIME')->nullable();
            $table->text('TICKER')->nullable();
            $table->text('O')->nullable();
            $table->text('H')->nullable();
            $table->text('L')->nullable();
            $table->text('C')->nullable();
            $table->text('GAP')->nullable();
            $table->text('ENTRY1')->nullable();
            $table->text('STOPLOST1')->nullable();
            $table->text('TSL1')->nullable();
            $table->text('TAGET11')->nullable();
            $table->text('TAGET21')->nullable();
            $table->text('TAGET31')->nullable();
            $table->integer('STATUS1')->nullable();
            $table->text('RSI1')->nullable();
            $table->text('PC1')->nullable();
            $table->text('FC1')->nullable();
            $table->text('LC1')->nullable();
            $table->text('ENTRY3')->nullable();
            $table->text('STOPLOST3')->nullable();
            $table->text('TSL3')->nullable();
            $table->text('TAGET13')->nullable();
            $table->text('TAGET23')->nullable();
            $table->text('TAGET33')->nullable();
            $table->integer('STATUS3')->nullable();
            $table->text('RSI3')->nullable();
            $table->text('PC3')->nullable();
            $table->text('FC3')->nullable();
            $table->text('LC3')->nullable();
            $table->text('ENTRY5')->nullable();
            $table->text('STOPLOST5')->nullable();
            $table->text('TSL5')->nullable();
            $table->text('TAGET15')->nullable();
            $table->text('TAGET25')->nullable();
            $table->text('TAGET35')->nullable();
            $table->integer('STATUS5')->nullable();
            $table->text('RSI5')->nullable();
            $table->text('PC5')->nullable();
            $table->text('FC5')->nullable();
            $table->text('LC5')->nullable();
            $table->integer('SHOW1')->nullable();
            $table->integer('SHOW3')->nullable();
            $table->integer('SHOW5')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signals');
    }
}
